var vid = document.getElementById('vid');
var playVideo = document.getElementById('playVideo');
var pauseVideo = document.getElementById('pauseVideo');
var volumeVideo = document.getElementById('volumeVideo');
var muteVideo = document.getElementById('muteVideo');
var volumeSeek = document.getElementById('volumeSeek');
var vidDuration = document.getElementById('vidDuration');
var vidCurrentDuration = document.getElementById('vidCurrentDuration');
var fullscreen = document.getElementById('fullscreen');
var slideSeek = document.getElementById('slideSeek');

/*video functions*/
vid.addEventListener('canplay', function() {
    pauseVideo.style.display = "none";
    muteVideo.style.display = "none";
    vidDuration.innerText = (timeConvert(vid.duration));
})

playVideo.addEventListener('click', function() {
    vid.play();
    playVideo.style.display = "none";
    pauseVideo.style.display = "";
})

pauseVideo.addEventListener('click', function() {
    vid.pause();
    playVideo.style.display = "";
    pauseVideo.style.display = "none";
})

volumeVideo.addEventListener('click', function() {
    vid.volume = 0;
    volumeVideo.style.display = "none";
    muteVideo.style.display = "";
    volumeSeek.value = 0;
})

muteVideo.addEventListener('click', function() {
    vid.volume = 1;
    volumeVideo.style.display = "";
    muteVideo.style.display = "none";
    volumeSeek.value = 100;
})

volumeSeek.addEventListener('change', function(){
   var seektoVolume = vid.volume * (volumeSeek.value / 100);
        if(vid.volume - 0.1 < 0) {
            vid.volume = 0;
            currentVolumPosition = vid.volume;
            volumeVideo.style.display = "none";
            muteVideo.style.display = "";
            return;
        }
        vid.volume = seektoVolume;
        currentVolumPosition = vid.volume;      
});

fullscreen.addEventListener('click', function(){
    vid.requestFullscreen();
})

slideSeek.addEventListener('change', function(){
    var seektoVideo = vid.duration * (slideSeek.value / 100);
    vid.currentTime = seektoVideo;
    playVideo.style.display = "none";
    pauseVideo.style.display = "";
})

vid.addEventListener('timeupdate', function() {
    var newTime = vid.currentTime * (100 / vid.duration);
    slideSeek.value = newTime;
    vidCurrentDuration.innerText = (timeConvert(vid.currentTime));
});
slideSeek.addEventListener('click', function() {
    playVideo.style.display = "none";
    pauseVideo.style.display = "";
})

function timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return  rhours + "." + rminutes ;
}



/*dark mode start*/
var toggleButton = document.getElementById('toggleButton');
var watchConteiner = document.getElementById('watchConteiner');
var comments = document.getElementById('comments');
var upnext = document.getElementById('upnext');
var header = document.getElementById('header');
var profileMenu = document.getElementById('profileMenu')
var input = document.getElementById('input');
var fasearch = document.getElementById('fasearch');

var shako = localStorage.getItem('darkMode');



toggleButton.innerHTML = shako

if(shako == 1) {
    toggleButton.innerHTML = '<i class="fas fa-toggle-on" id="toggleOn"></i>';
    header.classList.add('darker');
    profileMenu.classList.add('darker');
    watchConteiner.classList.add('darker');
    upnext.classList.add('darker');
    burger.classList.add('darker');
    openVideoButton.classList.add('darker');
    openVideoAppsButton.classList.add('darker');
    openMessagesButton.classList.add('darker');
    openNotificationsButton.classList.add('darker');
    menu_click.classList.add('darker');
    input.classList.add('darker');
    fasearch.classList.add('darker');

} else if (shako == 0) {
    toggleButton.innerHTML = '<i class="fas fa-toggle-off" id="toggleOff"></i>';
    header.classList.remove('darker');
    profileMenu.classList.remove('darker');
    watchConteiner.classList.remove('darker');
    upnext.classList.remove('darker');
    burger.classList.remove('darker');
    openVideoButton.classList.remove('darker');
    openVideoAppsButton.classList.remove('darker');
    openMessagesButton.classList.remove('darker');
    openNotificationsButton.classList.remove('darker');
    menu_click.classList.remove('darker');
    input.classList.remove('darker');
    fasearch.classList.remove('darker');
 }
 else {
    toggleButton.innerHTML = '<i class="fas fa-toggle-off" id="toggleOff"></i>';
}
toggleButton.addEventListener('click',function(){
    if(shako = localStorage.getItem('darkMode')) {
        if(shako == 1) {
            toggleButton.innerHTML = '<i class="fas fa-toggle-off" id="toggleOff"></i>';
            header.classList.remove('darker');
            profileMenu.classList.remove('darker');
            watchConteiner.classList.remove('darker');
            upnext.classList.remove('darker');
            burger.classList.remove('darker');
            openVideoButton.classList.remove('darker');
            openVideoAppsButton.classList.remove('darker');
            openMessagesButton.classList.remove('darker');
            openNotificationsButton.classList.remove('darker');
            menu_click.classList.remove('darker');
            fasearch.classList.remove('darker');

            input.classList.remove('darker');
            shako = 0;
            localStorage.setItem('darkMode',shako);
        } else {
                shako = 1;
                toggleButton.innerHTML = '<i class="fas fa-toggle-on" id="toggleOn"></i>';
                header.classList.add('darker');
                profileMenu.classList.add('darker');
                watchConteiner.classList.add('darker');
                upnext.classList.add('darker');
                burger.classList.add('darker');
                openVideoButton.classList.add('darker');
                openVideoAppsButton.classList.add('darker');
                openMessagesButton.classList.add('darker');
                openNotificationsButton.classList.add('darker');
                menu_click.classList.add('darker');
                input.classList.add('darker');
                fasearch.classList.add('darker');

                localStorage.setItem('darkMode',shako);
            }
    } else {
            shako = 1;
            toggleButton.innerHTML = '<i class="fas fa-toggle-on" id="toggleOn"></i>';
            header.classList.add('darker');
            profileMenu.classList.add('darker');
            watchConteiner.classList.add('darker');
            upnext.classList.add('darker');
            burger.classList.add('darker');
            openVideoButton.classList.add('darker');
            openVideoAppsButton.classList.add('darker');
            openMessagesButton.classList.add('darker');
            openNotificationsButton.classList.add('darker');
            menu_click.classList.add('darker');
            input.classList.add('darker');
            fasearch.classList.add('darker');
            
            localStorage.setItem('darkMode',shako);
        }
         
}) 
/*dark mode finish*/